import { Component, Input } from '@angular/core';
import { MedicineService } from '../medicine.service';

@Component({
  selector: 'app-medication-form',
  templateUrl: './medication-form.component.html',
  styleUrls: ['./medication-form.component.css']
})
export class MedicationFormComponent {

  @Input() username = '';
  @Input() password = '';
  @Input() term = '';
  medicines = false;
  error = '';
  constructor(private medicineService: MedicineService) {}

  search(): void {
    if (this.username && this.password) {
      const source = this.medicineService
        .getMedication(this.username, this.password, this.term)
        .subscribe(data => {
          this.medicines = data;
          this.error = '';
        },
        err => {
          this.medicines = false;
          switch (err.status) {
            case 401:
              this.error = `Incorrect credentials for ${this.username}`;
              break;
            case 404:
              this.error = `No medicine found for the term: "${this.term}"`;
              break;
            case 400:
              this.error = 'Bad Request: please check your input';
              break;
            case 500:
              this.error = 'Server Error: We are looking into the issue';
              break;
          }
        }
      );
    } else {
      this.error = 'Username and password cannot be blank';
    }
  }

}
