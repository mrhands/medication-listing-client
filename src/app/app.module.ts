import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MedicineService } from './medicine.service';
import { AppComponent } from './app.component';
import { MedicationFormComponent } from './medication-form/medication-form.component';


@NgModule({
  declarations: [
    AppComponent,
    MedicationFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [MedicineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
