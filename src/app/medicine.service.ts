import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';

import { Medicine } from './medicine';

@Injectable()
export class MedicineService {

  constructor(private http: HttpClient) {
  }

  getMedication(username: string, password: string, term: string): Observable<any> {
    return this.http.get<Medicine[]>(
      `//localhost:8080/search_medication?username=${username}&password=${password}&medication_list=${term}`
    ).debounceTime(1000);
  }

}
