export class Medicine {
    id: number;
    displayName: string;
    appearance: string;
    doseType: string;
  }
